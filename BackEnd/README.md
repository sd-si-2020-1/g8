# Membros
### Alunos:

Pedro Henrique Fernandes dos Reis - https://gitlab.com/PedroHe - (Líder do projeto)

# Motivação do Projeto

Esse jogo tem o potencial de ajudar a criança quando ela estiver diante da matemática, para poder fazer previsões, resolver problemas de sequências numéricas e de repetição. 

# Descrição do Projeto

O jogo consiste em uma tabela (matriz), na qual cada linha representa um início de sequência de cores e o jogador tem que adivinhar o restante da sequência de cada linha, com base no padrão encontrado no início. Por exemplo: se a sequência incial é verde, roxo, verde e roxo, deve se continuar o padrão com verde, roxo, ...

# Requerimentos

- NodeJS
- npm
- GNU/Linux distribution
- git
- MongoDB
- Open API com Swagger
- Jest

# Como executar o projeto

* 1º Passo: Digitar "git clone https://gitlab.com/sd-si-2020-1/g8.git" pelo terminal linux, para clonar o projeto
* 2º Passo: Digitar "cd g8/BackEnd/" no terminal, para entrar na pasta do projeto
* 3º Passo: Digitar "npm install" no terminal, para instalar todas as dependências
* 4º Passo: Digitar "node app.js start" no terminal, para iniciar a aplicação
* 5º Passo: Digitar "http://localhost:8080/docs/swagger" no navegador, para acessar a documentação swagger

# Como executar os testes do projeto

* 1º Passo: Digitar "npm run test" no terminal