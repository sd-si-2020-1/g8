test('open only one session', () => {
    var {Session} = require('../../models/');
    var {SessionAlreadyOpenedError} = require('../../models/errors');
    var s1 = Session.open('TesteDeSessao1');
    expect(()=>{Session.open('TesteDeSessao1')}).toThrowError(SessionAlreadyOpenedError);
})