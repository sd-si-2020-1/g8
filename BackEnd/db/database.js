db = {
  mongoose: null,
  autoIncrement: null,
  Schema: null,
  connection: null,  
  connect: function() {
    if (! this.connection) {
      try {
        this.mongoose = require('mongoose');
        this.mongoose.set('useUnifiedTopology', true)
        this.mongoose.set('useNewUrlParser', true)
        this.mongoose.set('useCreateIndex', true)
        this.mongoose.set('useFindAndModify', false)
        const config = require('../app/config')  
        const uri = "mongodb://"+config.db_host+"/"+config.db_name;
        this.connection = this.mongoose.createConnection(uri);
        const { Schema } = this.mongoose;
        this.Schema = Schema;
        this.autoIncrement = require('mongoose-auto-increment');
        this.autoIncrement.initialize(this.connection)        
      } catch (e) {
        throw new Error("Could not connect to database: "+e)
      }
    }
    return this;
  }
}
module.exports = db