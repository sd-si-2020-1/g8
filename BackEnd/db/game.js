const db = require('./database').connect();
var { sequencia_cores } = require('../models/game')

const gameSchema = new db.Schema({
  game: { type: db.Schema.Types.ObjectId, ref: 'Game' },
  status: { type: String, default: sequencia_cores.status },
  next: { type: Number, default: sequencia_cores.next },
  qtdEscolhas: { type: Number, default: sequencia_cores.qtdEscolhas },
  qtdCoresDaSequenciaInicial: { type: Number, default: sequencia_cores.qtdCoresDaSequenciaInicial },
  score: { type: Number, default: sequencia_cores.score },
  board: { 
    type: Array, 
    default: sequencia_cores.board
  } 
});
gameSchema.plugin(db.autoIncrement.plugin, 'Game');
gameSchema.methods.escolheCor = sequencia_cores.escolheCor;
gameSchema.methods.calculaAcertoCor = sequencia_cores.calculaAcertoCor;
gameSchema.methods.criaBoardComCoresRandomizadas = sequencia_cores.criaBoardComCoresRandomizadas;
gameSchema.methods.geraCorRandomizada = sequencia_cores.geraCorRandomizada;

/**
 * @class Game
 * @classdesc db/Game class: models Game operations with persistence
 * @augments moongose/Model
 * @augments models/Game
 * @see {@link https://mongoosejs.com/docs/api/model.html|mongoose/Model}
 * @see {@link models.md#Game|models/Game}  
 */
module.exports = db.connection.model('Game', gameSchema);