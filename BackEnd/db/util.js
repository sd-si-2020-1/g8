/**
 * useful functions
 * @module db/utils
*/


/**
 * list objects in database at console
 * @param {mongoose/Model} a mongoose Model
 * @example
 * // list objects of Game collection
 * listdb(Game) 
*/ 
listdb=(db)=>{db.find().exec().then((obj)=>console.log(obj))}

/** exports listdb function */
module.exports.listdb = listdb