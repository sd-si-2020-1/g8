/**
 * useful functions
 * @module app/utils
*/

// general requirements
const config = require('./config')
const jwt = require('jsonwebtoken');
const crypto = require('crypto')

/**
 * execute command in shell
 * @param {string} command to be executed 
 * @example
 * // list /etc directory
 * $('ls /etc')
 * ... contents of /etc directory
*/ 
$ = (cmd)=>{ child_process.exec(cmd,(err,out)=>{console.log(out)}) }

/**
 * encrypt a text with SHA256 algorith
 * @param {string} plain text to be encrypted
 * @returns {string} encrypted information
 * @see {@link https://nodejs.org/api/crypto.html#crypto_crypto|crypto}
 */
encrypt = (txt) => crypto.createHmac('sha256', config.secret)
                    .update(txt).digest('hex');

/**
 * sign and decode a object with JWT
 * @param {object} javascript object - payload
 * @returns {string} token encoded with JWT
 */
jwt_sign = (payload) => { return jwt.sign(payload, config.secret) }

/**
 * verify a JWT token
 * @param {string} JWT token
 * @returns {object} token decoded
 * @throws {object} JsonWebTokenError if token signature is wrong
 */
jwt_verify = (token) => { return jwt.verify(token, config.secret) }

/** exports function '$' to execute command in shell */
module.exports.$ = $

/** exports function encrypt (SHA256) */
module.exports.encrypt = encrypt

/** exports function jwt_sign */
module.exports.jwt_sign = jwt_sign

/** exports function jwt_verify */
module.exports.jwt_verify = jwt_verify
