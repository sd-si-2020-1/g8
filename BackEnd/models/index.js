/**
 * Models module
 * @module models
 */

const { Game, sequencia_cores } = require('./game')
const { Player } = require('./player')
const { Session } = require('./session')

/** export models/Game class */
module.exports.Game = Game

/** export models/sequencia_cores object */
module.exports.sequencia_cores = sequencia_cores

/** export models/Player class */
module.exports.Player = Player

/** export models/Session class */
module.exports.Session = Session