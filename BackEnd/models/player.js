const crypto = require('crypto');
const { encrypt } = require('../app/.')

/**
 * Class representing a sequencia_cores player
 */
class Player {
  login = null;
  password = null;
  
  /**
   * performs a set of login
   * @param {string} login - the login
   */
  set_login(login) {
    this.login = login
  }

  /**
   * performs a set of password
   * @param {string} pwd - the password
   */
  set_pwd(pwd) {
    this.password = pwd
  }  

  // TODO: document Player methods
	change_pwd(pwd_txt) {
    this.password = encrypt(pwd_txt)
  }
	check_pwd(attempt) {
    return (attempt) === this.password ? true : false;
	}
}

player = new Player()
module.exports.player = player
module.exports.Player = Player