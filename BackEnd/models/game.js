/** 
 * Class representing sequencia_cores game
*/

class Game {
	board = this.criaBoardComCoresRandomizadas();
	status = 'aberto';
	next = 0;
	qtdEscolhas = 0;
	qtdCoresDaSequenciaInicial = 0;	
	score = 0;

  /**
   * performs a choice of color on the board
   * @param {string} cor - the color to perform the choice
   */
  // TODO: document errors	
	escolheCor(cor) {
		if(this.status == 'fechado')
			throw new Error('Jogo está fechado');

		if(this.qtdEscolhas == 0)
			for(let j = 0; j < this.board[this.next].length; j++)
				if(this.board[this.next][j] != null)
					this.qtdCoresDaSequenciaInicial++;						
		
		for(let i = 0; i < this.board[this.next].length; i++) {				
			if(this.board[this.next][i] == null) {
				if(
					cor != 'rosa' && cor != 'amarelo' && cor != 'laranja' && 
					cor != 'vermelho' && cor != 'verde' && cor != 'roxo')
						throw new Error('Essa cor é inválida. Só é aceito rosa, ou amarelo, ou laranja, ou vermelho, ou verde, ou roxo');
				this.board[this.next][i] = cor;	
				if(this.calculaAcertoCor(cor)) {
					console.log('Parabéns, você acertou a cor');
					this.score++;						
				}
				else
					console.log('Infelizmente você não acertou a cor');					
				this.qtdEscolhas++;
				break;							
			
			}
		
		}
		if(!this.board[this.next].includes(null)) {
			console.log('Etapa concluída!');
			this.next++;
			this.qtdEscolhas = 0;
			this.qtdCoresDaSequenciaInicial = 0;
		}
		if(this.next > 0) {
			console.log("Jogo finalizado");
			this.status = 'fechado';
		}
					
	}

  /**
   * calculates the current color choice
   * @param {string} cor - the color to perform the choice
   * @returns {integer} - the right choice (1) or the wrong choice (0)
   */	
	calculaAcertoCor(cor) {
		let pos = this.qtdEscolhas;

		while(pos >= this.qtdCoresDaSequenciaInicial)
			pos = pos - this.qtdCoresDaSequenciaInicial;
			
		if(this.board[this.next][pos] == cor)
			return 1;
		else
			return 0;
		
	}

  /**
   * creates the board with random colors
   * @returns {Array} - the board game
   */	
	criaBoardComCoresRandomizadas() {
		let n = 1
		let board = Array(n).fill(null)

		for(let i = 0; i < n; i++) {
			board[i] = Array(12).fill(null)			
			for(let j = 0; j < 4; j++)
				board[i][j] = this.geraCorRandomizada()
		}
		return board
	}

  /**
   * generates a random color that comes from one of the six valid colors
   * @returns {string} - the random color
   */	
	geraCorRandomizada() {
		let cores = ['laranja', 'amarelo', 'verde', 'roxo', 'rosa', 'vermelho']
		let cor = null
		let min = Math.ceil(0)
		let max = Math.floor(6)

		// Nessa função random, o valor gerado não será menor que min e será menor, mas não igual, a max
		// Gera um número entre 0 a 5 para mapear as cores do array em valores relacionados a sua posição
		let random = Math.floor(Math.random() * (max-min)) + min
0
		switch(random) {
			case 0:
				cor = cores[0];
				break;
			case 1:
				cor = cores[1];
				break;
			case 2:
				cor = cores[2];
				break;
			case 3:
				cor = cores[3];
				break;
			case 4:
				cor = cores[4];
				break;
			case 5:
				cor = cores[5];
				break;	
			default:
				console.log("Não há correspondência do valor "+random +" com alguma cor");
				break;		
		}	
			
		return cor
	}
		
}

sequencia_cores = new Game()
module.exports.sequencia_cores = sequencia_cores
module.exports.Game = Game
