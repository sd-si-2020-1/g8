var { Session } = require('../db');
var { jwt_sign } = require('../app/');

/** 
 * @swagger
 * tags:
 *  name: Session
 *  description: operations with Sessions
 */

/**
 * @swagger
 * definitions:
 *    Session:
 *      type: object
 *      properties:
 *        auth:
 *          type: boolean
 *          description: status authentication of session, true or false
 *        token:
 *          type: string
 *          description: JWT encoded token, null if not ok
*/

function setup(server) {
/**
 * @swagger
 *
 * /session/open:
 *   post:
 *     tags:
 *       - Session
 *     description: open a new session
 *     parameters:
 *       - in: body
 *         name: credentials
 *         description: login and password
 *         required:
 *           - login
 *           - password
 *         schema:
 *           type: object
 *           properties:
 *             login:
 *               type: string
 *             password:
 *               type: string
 *     responses:
 *       200:
 *         description: new session
 *         schema:
 *           $ref: '#/definitions/Session'
 */
server.post('/session/open', 
  (req, res, next) => {
    Session.open(
      req.body.login, 
      req.body.password,  
      (err,session) => {          
        if (session) {         
          session.save();
          var token = jwt_sign({id: session.id}) 
          res.send({
            auth: true,
            token
          })
        } else {
          res.send({
            auth: false,
            token: null
          })
        }
      } 
    );
    next();
  });
}

module.exports = setup;