rest = {
  server: null,
  docs: null,
  start: function() {
    // FIXME: check if server is already started
    var restify = require('restify');
    var sequencia_cores_rest = require('./sequencia_cores')
    var game_rest = require('./game')
    var session_rest = require('./session')
    var player_rest = require('./player')    
    this.server = restify.createServer();
    this.server.use(restify.plugins.bodyParser());    
    sequencia_cores_rest(this.server)
    game_rest(this.server)
    session_rest(this.server)  
    player_rest(this.server)  
    config = require('../app/config')
    const corsMiddleware = require('restify-cors-middleware')
    const cors = corsMiddleware({
      origins: config.cors_origins
    })
    this.server.pre(cors.preflight)
    this.server.use(cors.actual)
    this.server.listen(config.rest_port, function(error) {
      // FIXME: handle listen error
      console.log('listening at port %s', config.rest_port);
    });
    return this;    
  }, // TODO: implement stop method
  start_docs: function() {
    var restifySwaggerJsdoc = require('restify-swagger-jsdoc');
    restifySwaggerJsdoc.createSwaggerPage({
        title: 'API documentation', // Page title
        version: '1.0.0', // Server version
        server: this.server, // Restify server instance created with restify.createServer()
        path: '/docs/swagger', // Public url where the swagger page will be available
        apis: [ './rest/sequencia_cores.js', './rest/game.js', './rest/session.js', './rest/player.js' ],
    });
    return true;
  }
}

module.exports = rest