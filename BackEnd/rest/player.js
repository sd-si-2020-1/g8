var { Player } = require('../db');

/**
 * @swagger
 * tags:
 *  name: Player
 *  description: operations with Sequencia Cores Player
 */

function create(req, res, next) {
	player = new Player()
	player.save().then(
	  ()=>{
		res.send(player)
		next();
	  }
	).catch(
	  (err)=>{next(err)}
	)
}

function editLogin(req, res, next) {
    var id = parseInt(req.params.id)
    Player.findById(id).exec()
    .then(
        (player) => {
            try {
                player.set_login(req.params.login)
                player.save().then(
                    () => {
                        res.send(player)
                        next();    
                    }
                )    
            }catch(err) {
                next(err)
            }
        }

    ).catch(
		(err) => { next(err) }
	)
}

function editPwd(req, res, next) {
    var id = parseInt(req.params.id)
    Player.findById(id).exec()
    .then(
        (player) => {
            try {
                player.set_pwd(req.params.pwd)
                player.save().then(
                    () => {
                        res.send(player)
                        next();    
                    }
                )    
            }catch(err) {
                next(err)
            }
        }

    ).catch(
		(err) => { next(err) }
	)
}

function setup(server) {
/**
 * @swagger
 *
 * /player/create:
 *   get:
 *     tags:
 *       - Player
 *     description: creates a new player
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: the new player created
 */
server.get('/player/create', create);

/**
 * @swagger
 *
 * /player/{id}/editLogin/{login}:
 *   get:
 *     tags:
 *       - Player
 *     description: edits a player by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: player id
 *         required: true
 *         type: integer 
 *       - name: login
 *         in: path
 *         description: login of the player
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: the login player was edited
 */
server.get('/player/:id/editLogin/:login', editLogin)

/**
 * @swagger
 *
 * /player/{id}/editPwd/{pwd}:
 *   get:
 *     tags:
 *       - Player
 *     description: edits a player by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: player id
 *         required: true
 *         type: integer 
 *       - name: pwd
 *         in: path
 *         description: pwd of the player
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: the pwd player was edited
 */
server.get('/player/:id/editPwd/:pwd', editPwd)

}

module.exports = setup