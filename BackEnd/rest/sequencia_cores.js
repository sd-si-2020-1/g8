var { sequencia_cores } = require('../models/game.js');

/**
 * @swagger
 * tags:
 *  name: Sequencia Cores
 *  description: operations with Sequencia Cores
 */

function board(req, res, next) {
 	res.send(sequencia_cores.board);
 	next();
}

function status(req, res, next) {
	res.send(sequencia_cores.status);
	next();
}


function status_all(req, res, next) { 
	const { status, score } = sequencia_cores;
	res.send({status, score })
	  next();
}
  

function score(req, res, next) {
	res.send(sequencia_cores.score.toString());
	next();
}

function escolheCor(req, res, next) {
	try{
		sequencia_cores.escolheCor(req.params.cor);
	} catch(e){
		return next(e);	
	}
	res.send(sequencia_cores.board);
	next();
}

function routes(server) {
/**
 * @swagger
 *
 * /sequencia_cores/board:
 *   get:
 *     tags:
 *       - Sequencia Cores
 *     description: returns the board game
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: current board game
 */
server.get('/sequencia_cores/board', board);

/**
 * @swagger
 *
 * /sequencia_cores/status:
 *   get:
 *     tags:
 *       - Sequencia Cores
 *     description: returns the status game
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: current status game
 */
server.get('/sequencia_cores/status', status);

/**
 * @swagger
 *
 * /sequencia_cores/status/all:
 *   get:
 *     tags:
 *       - Sequencia Cores
 *     description: returns the status game and the score game
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: current status game and the score game
 */
server.get('/sequencia_cores/status/all', status_all);

/**
 * @swagger
 *
 * /sequencia_cores/score:
 *   get:
 *     tags:
 *       - Sequencia Cores
 *     description: returns the player score
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: current score game
 */
server.get('/sequencia_cores/score', score);

/**
 * @swagger
 *
 * /sequencia_cores/escolheCor/{cor}:
 *   get:
 *     tags:
 *       - Sequencia Cores
 *     description: make a color choice
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: cor
 *         in: path
 *         description: color choice by player ('rosa' or 'amarelo' or 'laranja' or 'vermelho' or 'verde' or 'roxo')
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: current board game
 */
server.get('/sequencia_cores/escolheCor/:cor', escolheCor);
}

module.exports = routes

