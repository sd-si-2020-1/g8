var { Game } = require('../db/game.js');

/**
 * @swagger
 * tags:
 *  name: Game
 *  description: operations with Sequencia Cores Games
 */

/**
 * @swagger
 * definitions:
 *    Game:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *          description: the auto-incremented id of the game
 *        status:
 *          type: string
 *          description: game status, can be 'aberto' or 'fechado'
 *        next:
 *          type: number
 *          description: next row of the table, can be 0 ... * (until the max possible)
 *        qtdEscolhas:
 *          type: number
 *          description: amount of color choices made by the player in each row of the table
 *        qtdCoresDaSequenciaInicial:
 *          type: number
 *          description: number of colors of the initial sequence in each row of the table
 *        score:
 *          type: number
 *          description: game score, can be 0 ... * (until the max possible)
 *        board:
 *          type: array
 *          items:
 *            type: string
 *          minItems: 12
 *          maxItemns: 48
 *          description: board game, a array with 48 positions, divided into 4 arrays, from 0 to 11 each. 
*/

function board(req, res, next) {
	var id = parseInt(req.params.id)
	Game.findById(id).exec()
	.then(
		(game) => { res.send(game.board) }
	).catch(
		(err) => { next(err) }
	)
 	next();
}

function status(req, res, next) {
	var id = parseInt(req.params.id)
	Game.findById(id).exec()
	.then(
		(game) => { res.send({status: game.status}) }
	).catch(
		(err) => { next(err) }
	)
	next();
}

function score(req, res, next) {
	var id = parseInt(req.params.id)
	Game.findById(id).exec()
	.then(
		(game) => { res.send({score: game.score}) }
	).catch(
		(err) => { next(err) }
	)	
	next();
}

function escolheCor(req, res, next) {
	var id = parseInt(req.params.id)
	Game.findById(id).exec()
	.then(
		(game) => {
			try {
				game.escolheCor(req.params.cor)
				game.markModified('board')
				game.save().then(
					()=>{
					  res.send(game.board)
					  next();    
					}
				  )		
			} catch(err) {
				next(err)
			}		
		}
	).catch(
		(err) => { next(err) }
	)
}

function create(req, res, next) {
	game = new Game()
	game.save().then(
	  ()=>{
		res.send(game)
		next();
	  }
	).catch(
	  (err)=>{next(err)}
	)
}

function routes(server) {
/**
 * @swagger
 *
 * /game/create:
 *   get:
 *     tags:
 *       - Game
 *     description: creates a new game
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: the new game created
 */
server.get('/game/create', create);

/**
 * @swagger
 *
 * /game/{id}/board:
 *   get:
 *     tags:
 *       - Game 
 *     description: returns the board game identified by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: game id
 *         required: true
 *         type: integer 
 *     responses:
 *       200:
 *         description: current board game identified by id
 */
server.get('/game/:id/board', board);

/**
 * @swagger
 *
 * /game/{id}/status:
 *   get:
 *     tags:
 *       - Game
 *     description: returns the game status identified by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: game id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: current game status identified by id
 */
server.get('/game/:id/status', status);

/**
 * @swagger
 *
 * /game/{id}/score:
 *   get:
 *     tags:
 *       - Game
 *     description: returns the game score identified by id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: game id
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: current game score identified by id
 */
server.get('/game/:id/score', score);

/**
 * @swagger
 *
 * /game/{id}/escolheCor/{cor}:
 *   get:
 *     tags:
 *       - Game
 *     description: make a color choice
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         in: path
 *         description: game id
 *         required: true
 *         type: integer
 *       - name: cor
 *         in: path
 *         description: color choice by player ('rosa' or 'amarelo' or 'laranja' or 'vermelho' or 'verde' or 'roxo')
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: current board game
 */
server.get('/game/:id/escolheCor/:cor', escolheCor);
}

module.exports = routes

