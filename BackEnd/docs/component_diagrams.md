# Diagrama de componentes

Veja também: [Manual do desenvolvedor](README.md)

## Componentes do backend

```plantuml
@startuml backend_components
node backend <<nodejs app>> {

  note left of models
    provide business 
    logic  
  end note
  package "models" <<module>> {
    [Game, Player, etc...] as models_classes <<classes>>
  }

  note left of db
    provide Schema 
    and access to 
    database API
  end note
  package "db" <<module>> {
    [Game, Player and etc...] as db_classes <<classes>>
    [db] as db_object <<object>>
    db_object -left-> db_classes : use <<require>>
  }  
  db_classes -down-> models_classes : use <<import methods>>
  
  package "rest" <<module>> {
    [rest] as rest_object <<object>>
    [Game, Player and etc...]  as rest_classes <<classes>>
    rest_object -left-> rest_classes : controls setup <<require>>
  }
  note left of rest
    provide REST interface 
    and document API 
    with Swagger/OpenAPI
  end note

  package "app" <<module>> {
    [config] as config_object <<object>>
  }

  db_object -down-> config_object : use <<require>>
  rest_object -down-> config_object : use <<require>>

  db_classes <-up- rest_classes : use <<request>>
  node "Mongoose" <<ODM framework>> {
  }
  interface "db_api" as db_api <<library api>>
  db_api -right- Mongoose
}

node "app.js" <<nodejs script>> {
}
app.js <-down-> rest_object : control <<start, stop>>

interface "rest api" as rest_api <<http protocol>>
interface "api docs" as rest_docs <<http protocol>>
rest_api -down- rest_object
rest_docs -down- rest_object

database MongoDB <<Document-based database>> { 
  package "SequenciaCores db" <<database>> {
    [collections...] as collections <<mongo collections>>
  }
}

Mongoose -down- MongoDB : control <<mongodb protocol>>
db -down-> db_api : use <<require>>
@enduml
```

## Componentes do módulo models

```plantuml
@startuml models
  package "models" { 
    [Game] as GameModel <<class>>
    [Player] as PlayerModel <<class>> 
    [Session] as SessionModel <<class>>
    GameModel --> SessionModel : use 1 instance\n <<internal logic>>    
    SessionModel --> PlayerModel : use 1 instance\n <<internal logic>>
  }
@enduml
```

## Componentes do módulo db

```plantuml
@startuml db
  package "db" {
    [Game] as GameDB <<class>>
    [Player] as PlayerDB <<class>> 
    [Session] as SessionDB <<class>>
    [db] as Database <<object>>
    GameDB -down-> Database : use <<require>>
    SessionDB -down-> Database : use <<require>>    
    PlayerDB -down-> Database : use <<require>>
    GameDB -down-> SessionDB : use 1 instance\n <<internal logic>>
    SessionDB -down->PlayerDB: use 1 instance\n <<internal logic>>    
  }
@enduml
```