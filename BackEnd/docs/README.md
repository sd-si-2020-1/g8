# Manual do desenvolvedor

# Arquitetura do backend

## Geral
```plantuml
actor jogador
node frontend <<ReactNative\nou React>> { 
}
jogador <-right-> frontend: <<celular ou web>>\ninterface
node backend <<aplicação\nNodejs>> {
}
frontend <-right-> backend: <<padrão REST>>\nHTTP
node MongoDB <<banco de dados\nbaseado em documento>> {
}
backend <-right-> MongoDB: <<MongoDB>>\nprotocolo
```

## Documentação UML
* [Casos de uso](use_cases.md)
* [Diagramas de componentes](component_diagrams.md)
* [Diagramas de sequência](sequence_diagrams.md)

## API dos módulos
* [models](models.md)
* [db](db.md)