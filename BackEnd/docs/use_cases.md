# Casos de uso

Veja também: [Manual do desenvolvedor](README.md)

## Diagrama de caso de uso: Papéis

```plantuml
@startuml papeis
left to right direction

actor "Visitante" as visitante
note top of visitante : qualquer um que\n acessa o sistema 

actor "Jogador" as jogador
note top of jogador : jogador é um usuário que\n efetuou login e já pode jogar

rectangle Backend {
  usecase "uc1 - cadastrar conta" as uc1
  usecase "uc2 - abrir sessão" as uc2
  usecase "uc3 - realizar escolha de cor" as uc3
}

visitante <|-- jogador

visitante --> uc1
visitante --> uc2
jogador --> uc3

@enduml
```

## Casos de uso textuais

| # | Título          | Descrição                                             | Pré-condições                              | Fluxo normal                                                | Fluxo alternativo                                                                                                                                 |
|---|-----------------|-------------------------------------------------------|--------------------------------------------|-------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | Cadastrar conta | como visitante,<br> gostaria de cadastrar minha conta | não há                   | - visitante fornece login e senha<br> - cadastro é efetuado | - caso 'login já exista':<br>  notifica e solicita outro                                                                                          |
| 2 | Abrir sessão    | como visitante,<br> gostaria de abrir uma sessão      | visitante já efetou cadastro anteriormente | - visitante fornece login e senha<br> - a senha do visitante é checada e validada<br> - a sessão é aberta<br> - é fornecido um token pelo jwt sign que autentica o usuário<br> | - caso 'senha errada':<br> notifica e pede para informar novamente<br> - caso 'sessão já aberta':<br> notifica que somente uma sessão é permitida |
| 3 | realizar escolha de cor| como jogador, realizo uma escolha de cor para a sequencia no tabuleiro | jogador possui sessão aberta| - jogador fornece a cor correspondente à sequencia prevista <br> - jogador recebe o tabuleiro atualizado com a escolha| - caso 'posição inválida ou jogo fechado': <br>- usuário é notificado do problema|