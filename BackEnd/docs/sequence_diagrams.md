# Diagramas de sequência

Veja também: [Manual do desenvolvedor](README.md)

## UC1 - Caso de uso 1: Cadastrar conta

### Fluxo normal

```plantuml
@startuml uc1_cadastrar_conta_fluxo_normal
title **UC1 - CADASTRAR CONTA - FLUXO NORMAL**
autonumber
actor Visitante
boundary Frontend
participant Player as PlayerREST <<rest module>>
participant Player as PlayerDB <<db module>>
Visitante -> Frontend : solicita cadastro\nde conta
Frontend -> PlayerREST : POST /player/create \n {"login": <login>, \n"password": <pwd>}
database Database
PlayerREST -> PlayerDB : verifica disponibilidade\nPlayer.findByLogin(<login>)
PlayerDB -> Database : find <login>\nna coleção players
Database -> PlayerDB : resultado <false>
PlayerDB -> PlayerREST: resultado <false>
PlayerREST -> PlayerDB : instancia\np = Player(<login>,<pwd>);\np.save()
PlayerDB -> Database: save <login, pwd>\nna coleção players
Database -> PlayerDB : resultado <true>
PlayerDB -> PlayerREST : resultado <true>
PlayerREST -> Frontend : RESPONSE 200\n{ "status": "sucess" }
Frontend -> Visitante : cadastro realizado\ncom sucesso
@enduml
```

### Erro de login existente

```plantuml
@startuml uc1_cadastrar_conta_erro_login_existe
title **UC1 - CADASTRAR CONTA - ERRO DE LOGIN EXISTENTE**
autonumber
actor Visitante
boundary Frontend
participant Player as PlayerREST <<rest module>>
participant Player as PlayerDB <<db module>>
Visitante -> Frontend : solicita cadastro\nde conta
Frontend -> PlayerREST : POST /player/create \n {"login": <login>, \n"password": <pwd>}
database Database
PlayerREST -> PlayerDB : verifica disponibilidade\nPlayer.findByLogin(<login>)
PlayerDB -> Database : find <login>\nna coleção players
Database -> PlayerDB : resultado <true>
PlayerDB -> PlayerREST: resultado <true>
PlayerREST -> Frontend : RESPONSE 501\n{ "error": <ErrorLoginAlreadyExists> }
Frontend -> Visitante : login já existe, escolha outro
@enduml
```

## UC2 - Caso de uso 2: Abrir sessão

### Fluxo normal

```plantuml
@startuml uc2_abrir_sessao
title **UC2 - ABRIR SESSÃO - FLUXO NORMAL**
autonumber
actor Visitante
boundary Frontend
participant Session as SessionREST <<rest module>>
participant Player as PlayerDB <<db module>>
participant Session as SessionDB <<db module>>
Visitante -> Frontend : efetua login no sistema
Frontend -> SessionREST : POST /session/open \n {"login": <login>, \n"password": <pwd>}
SessionREST -> PlayerDB : verifica a senha\nPlayer.check_pwd(<login,pwd>)
PlayerDB -> Database : localiza o jogador pelo login\nPlayer.findByLogin(<login>)
Database -> PlayerDB : resultado:\n<<object>><player>
PlayerDB -> PlayerDB : verifica a senha\n<player>.check_pwd(<pwd>)
PlayerDB -> SessionREST : resultado:\n<<object>><player>
SessionREST -> SessionDB : abre sessão \nSession.open(<player>)
SessionDB -> SessionDB : verifica se já tem sessão aberta\nSession.isOpened(<player>)
SessionDB -> SessionDB : resultado <false>
SessionDB -> Database : instancia <session> e salva <session>.save()s
Database -> SessionDB : É fornecido um token pelo jwt que autentica a sessão do usuário
Database -> SessionDB : resultado ok
SessionDB -> SessionREST : resultado: \n<<object>> <session>
SessionREST -> Frontend : RESPONSE 200\n{ "auth": true, "token": token }\n<<jwt token>>
Frontend -> Visitante : login realizado\ncom sucesso 
@enduml
```

### Erro de senha incorreta

```plantuml
@startuml uc2_abrir_sessao__erro_login_senha_incorreta
title **UC2 - ABRIR SESSÃO - ERRO DE LOGIN SENHA INCORRETA**
autonumber
actor Visitante
boundary Frontend
participant Session as SessionREST <<rest module>>
participant Player as PlayerDB <<db module>>
participant Session as SessionDB <<db module>>
Visitante -> Frontend : efetua login no sistema
Frontend -> SessionREST : POST /session/open \n {"login": <login>, \n"password": <pwd>}
SessionREST -> PlayerDB : verifica a senha\nPlayer.check_pwd(<login,pwd>)
PlayerDB -> Database : localiza o jogador pelo login\nPlayer.findByLogin(<login>)
Database -> PlayerDB : resultado:\n<<object>><player>
PlayerDB -> PlayerDB : verifica a senha\n<player>.check_pwd(<pwd>)
PlayerDB -> SessionREST : resultado:\n<false>
SessionREST -> Frontend : RESPONSE 501\n{ "error": <ErrorPassWordIsIncorrect> }
Frontend -> Visitante : login não realizado\nsenha incorreta
@enduml
```

## UC3 - Caso de uso 3: Realizar escolha de cor

### Fluxo normal

```plantuml
@startuml uc3_realizar_escolha_cor
title **UC3 - REALIZAR ESCOLHA DE COR - FLUXO NORMAL**
autonumber
actor Jogador
boundary Frontend
participant Game as GameREST <<rest module>>
participant Game as GameDB <<db module>>
Jogador -> Frontend : reliza escolha de cor
Frontend -> GameREST : GET\n/game/<id>/escolheCor/<cor>
database Database
GameREST -> GameDB : obtém game identificado pelo id \nGame.findById(<id>)
GameDB -> Database : find <id>\nna coleção Games
Database -> GameDB : resultado <objeto game>
GameDB -> GameREST: resultado <objeto game>
GameREST -> GameDB : executa a escolha <cor> no <game object>\n<game object>.escolheCor(<cor>)
GameDB -> Database : salva escolha <game>.save()
Database -> GameDB : resultado ok
GameDB -> GameREST : resultado <objeto board>
GameREST -> Frontend : RESPONSE 200\n{ "response": <objeto board> }
Frontend -> Jogador : escolha realizada\ncom sucesso 
@enduml
```