## Modules

<dl>
<dt><a href="#module_models/errors">models/errors</a></dt>
<dd><p>Errors from models module</p>
</dd>
<dt><a href="#module_models">models</a></dt>
<dd><p>Models module</p>
</dd>
</dl>

## Classes

<dl>
<dt><a href="#Game">Game</a></dt>
<dd><p>Class representing sequencia_cores game</p>
</dd>
<dt><a href="#Player">Player</a></dt>
<dd><p>Class representing a sequencia_cores player</p>
</dd>
<dt><a href="#Session">Session</a></dt>
<dd><p>Class representing player session</p>
</dd>
</dl>

<a name="module_models/errors"></a>

## models/errors
Errors from models module


* [models/errors](#module_models/errors)
    * [~SessionAlreadyOpenedError([message])](#module_models/errors..SessionAlreadyOpenedError)
    * [~SessionAlreadyClosedError([message])](#module_models/errors..SessionAlreadyClosedError)
    * [~SessionNotOpenedError([message])](#module_models/errors..SessionNotOpenedError)

<a name="module_models/errors..SessionAlreadyOpenedError"></a>

### models/errors~SessionAlreadyOpenedError([message])
Session already open error

**Kind**: inner method of [<code>models/errors</code>](#module_models/errors)  
**See**: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error  

| Param | Type | Description |
| --- | --- | --- |
| [message] | <code>string</code> | custom message |

<a name="module_models/errors..SessionAlreadyClosedError"></a>

### models/errors~SessionAlreadyClosedError([message])
Session already closed error

**Kind**: inner method of [<code>models/errors</code>](#module_models/errors)  
**See**: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error  

| Param | Type | Description |
| --- | --- | --- |
| [message] | <code>string</code> | custom message |

<a name="module_models/errors..SessionNotOpenedError"></a>

### models/errors~SessionNotOpenedError([message])
Session not opened

**Kind**: inner method of [<code>models/errors</code>](#module_models/errors)  
**See**: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Error  

| Param | Type | Description |
| --- | --- | --- |
| [message] | <code>string</code> | custom message |

<a name="module_models"></a>

## models
Models module


* [models](#module_models)
    * [.Game](#module_models.Game)
    * [.sequencia_cores](#module_models.sequencia_cores)
    * [.Player](#module_models.Player)
    * [.Session](#module_models.Session)

<a name="module_models.Game"></a>

### models.Game
export models/Game class

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="module_models.sequencia_cores"></a>

### models.sequencia_cores
export models/sequencia_cores object

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="module_models.Player"></a>

### models.Player
export models/Player class

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="module_models.Session"></a>

### models.Session
export models/Session class

**Kind**: static property of [<code>models</code>](#module_models)  
<a name="Game"></a>

## Game
Class representing sequencia_cores game

**Kind**: global class  

* [Game](#Game)
    * [.escolheCor(cor)](#Game+escolheCor)
    * [.calculaAcertoCor()](#Game+calculaAcertoCor) ⇒ <code>integer</code>
    * [.criaBoardComCoresRandomizadas()](#Game+criaBoardComCoresRandomizadas) ⇒ <code>array</code>
    * [.geraCorRandomizada()](#Game+geraCorRandomizada) ⇒ <code>string</code>

<a name="Game+escolheCor"></a>

### game.escolheCor(cor)
performs a choice of color on the board

**Kind**: instance method of [<code>Game</code>](#Game)  

| Param | Type | Description |
| --- | --- | --- |
| cor | <code>string</code> | the color to perform the choice |

<a name="Game+calculaAcertoCor"></a>

### game.calculaAcertoCor() ⇒ <code>integer</code>
calculates the current color choice

**Kind**: instance method of [<code>Game</code>](#Game)  
| Param | Type | Description |
| --- | --- | --- |
| cor | <code>string</code> | the color to perform the choice |
**Returns**: <code>integer</code> - - the right choice (1) or the wrong choice (0)  

<a name="Game+criaBoardComCoresRandomizadas"></a>

### game.criaBoardComCoresRandomizadas() ⇒ <code>array</code>
creates the board with random colors

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: <code>array</code> - - the board game

<a name="Game+geraCorRandomizada"></a>

### game.geraCorRandomizada() ⇒ <code>string</code>
generates a random color that comes from one of the six valid colors

**Kind**: instance method of [<code>Game</code>](#Game)  
**Returns**: <code>string</code> - - the random color



<a name="Player"></a>

## Player
Class representing a sequencia_cores player

**Kind**: global class  
<a name="Session"></a>

## Session
Class representing player session

**Kind**: global class  

* [Session](#Session)
    * _instance_
        * [.close()](#Session+close)
    * _static_
        * [.open(player, password)](#Session.open) ⇒ <code>boolean</code>

<a name="Session+close"></a>

### session.close()
Close session

**Kind**: instance method of [<code>Session</code>](#Session)  
**Throws**:

- session already closed
- session not opened

<a name="Session.open"></a>

### Session.open(player, password) ⇒ <code>boolean</code>
Open session

**Kind**: static method of [<code>Session</code>](#Session)  
**Returns**: <code>boolean</code> - true - if session true if sucesfully opened; false - otherwise  
**Throws**:

- <code>SessionAlreadyOpened</code> - if a player try to open another session


| Param | Type |
| --- | --- |
| player | <code>object</code> | 
| password | <code>string</code> | 
