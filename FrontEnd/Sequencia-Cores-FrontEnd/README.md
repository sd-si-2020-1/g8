# Membros
### Alunos:

Pedro Henrique Fernandes dos Reis - https://gitlab.com/PedroHe - (Líder do projeto)

# Motivação do Projeto

Esse jogo tem o potencial de ajudar a criança quando ela estiver diante da matemática, para poder fazer previsões, resolver problemas de sequências numéricas e de repetição. 

# Descrição do Projeto

O jogo consiste em uma tabela (matriz), na qual cada linha representa um início de sequência de cores e o jogador tem que adivinhar o restante da sequência de cada linha, com base no padrão encontrado no início. Por exemplo: se a sequência incial é verde, roxo, verde e roxo, deve se continuar o padrão com verde, roxo, ...

# Requerimentos

- NodeJS
- npm/yarn
- GNU/Linux distribution
- git
- EXPO
- Visual Studio

# Como executar o projeto

* 1º Passo: Digitar "git clone https://gitlab.com/sd-si-2020-1/g8.git" pelo terminal linux, para clonar o projeto
* 2º Passo: Digitar "cd g8/FrontEnd/Sequencia-Cores-FrontEnd" no terminal, para entrar na pasta do projeto
* 3º Passo: Digitar "yarn" no terminal, para instalar todas as dependências
* 4º Passo: Digitar "expo start" no terminal, para iniciar a aplicação
* 5º Passo: Modificar a base_url do servidor local em src/services/api (para saber sua base_url, digite "ifconfig" no terminal e procure pelo ip do "inet")
* 6º Passo: Baixe o aplicativo do "expo" na play store, com o seu smartphone
* 7º Passo: Após baixar esse app, utilizar o Scan QR Code desse aplicativo e direcionar o seu smartphone no QR Code que está no terminal ou no navegador, devido ao comando "expo start"
* 8º Passo: Para funcionar o jogo, é preciso executar os passos do BackEnd e, ao abrir o projeto no seu smartphone, clicar no botao "Entrar" que vai direto para o jogo
* 9º Passo: Para reiniciar o jogo, é preciso reiniciar o servidor do BackEnd, digitando "node app.js start"
* 10º Passo: Dentro do jogo é possível jogar através dos botões abaixo das "Cores Disponíveis" e ao clicá-los aparecem no board de visualização acima. A pontuação é somada sempre que acerta a cor, sendo que para acertar tem que seguir as 4 cores iniciais que aparecem no board no começo do jogo. O status modifica de aberto para fechado quando o jogo termina.
