import React from 'react'
import { Container } from './styles'

export default function Square({color="#C4C4C4", left="0px", top="0px"}) {
    return(
      <Container background={color} lef={left} to={top}/>      
    );
}