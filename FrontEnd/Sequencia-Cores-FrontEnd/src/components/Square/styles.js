import styled from "styled-components/native";

export const Container = styled.View`
  position: absolute;
  width: 70px;
  height: 67px;
  background-color: ${props => props.background ? props.background : '#C4C4C4'};
  left: ${props => props.lef ? props.lef : "0px"};
  top: ${props => props.to ? props.to : "0px"};
  border-radius: 0px; 
`;