import React from 'react'
import { Container } from './styles'

export default function ButtonSquare({color="#C4C4C4", left="0px", top="0px",...props}) {
    return(
      <Container {...props} background={color} lef={left} to={top}/>      
    );
}