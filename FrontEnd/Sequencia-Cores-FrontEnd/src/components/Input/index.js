import React from 'react'
import { Container, InputContainer, Title, InputC } from './styles'

export default function Input({title}) {
  return (
    <Container>
      <InputContainer>
        <Title>{title}</Title>
        <InputC />
      </InputContainer>
    </Container>
  );
}