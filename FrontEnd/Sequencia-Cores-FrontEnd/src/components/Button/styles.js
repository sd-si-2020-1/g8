import styled from "styled-components/native";

export const Container = styled.TouchableOpacity`
  width: ${props => props.wid ? props.wid : "200px"};
  height: ${props => props.hei ? props.hei : "50px"};
  background-color: ${props => props.background ? props.background : '#456BF2'};
  justify-content: center;
  align-items: center;
`;

export const Title = styled.Text`
  color: #000;
  font-size: ${props => props.size ? props.size : "24px"};
`;