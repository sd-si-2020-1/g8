import React from 'react'
import { Container, Title } from './styles'

export default function Button({color="#456BF2" ,title, width = "200px", height = "50px", fontSize = "24px",...props}) {
  return (
    <Container {...props} background={color} wid={width} hei={height} >      
      <Title size={fontSize}>{title}</Title>
    </Container>
  );
}