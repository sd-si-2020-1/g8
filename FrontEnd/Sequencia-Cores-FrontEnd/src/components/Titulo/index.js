import React from 'react'
import { Container, Title } from './styles'

export default function Titulo({title, left="0px", top="0px"}) {
  return (
    <Container lef={left} to={top}>
        <Title>{title}</Title>
    </Container>
  );
}