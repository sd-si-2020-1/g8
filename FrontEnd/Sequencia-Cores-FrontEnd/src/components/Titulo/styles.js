import styled from "styled-components/native";

export const Container = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  text-align: center;
  left: ${props => props.lef ? props.lef : "0px"};
  top: ${props => props.to ? props.to : "0px"};  
`;

export const Title = styled.Text`
  color: #000;
  font-size: 24px;
`;
