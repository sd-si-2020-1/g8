import { combineReducers } from "redux";

import seq from "./Sequencia_Cores_Redux/reducer";

export default combineReducers({
    seq
});