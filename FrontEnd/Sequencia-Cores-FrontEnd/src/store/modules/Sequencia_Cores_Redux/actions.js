export function getStatusRequest() {
    return {
      type: "@seq/GET_STATUS_REQUEST",
    };
  }
  
export function getStatusSuccess(status, score) {
  return {
    type: "@seq/GET_STATUS_SUCCESS",
    payload: { status, score },
  };
}

export function getStatusFailure() {
  return {
    type: "@seq/GET_STATUS_FAILURE",
  };
}

export function getBoardRequest() {
  return {
    type: "@seq/GET_BOARD_REQUEST",
  };
}

export function getBoardSuccess(board) {
  return {
    type: "@seq/GET_BOARD_SUCCESS",
    payload: { board },
  };
}

export function getBoardFailure() {
  return {
    type: "@seq/GET_BOARD_FAILURE",
  };
}

export function escolheCorRequest(cor) {
  return {
    type: "@seq/ESCOLHECOR_REQUEST",
    payload: { cor },
  };
}

export function escolheCorSuccess() {
  return {
    type: "@seq/ESCOLHECOR_SUCCESS",
  };
}

export function escolheCorFailure() {
  return {
    type: "@seq/ESCOLHECOR_FAILURE",
  };
}
  

  