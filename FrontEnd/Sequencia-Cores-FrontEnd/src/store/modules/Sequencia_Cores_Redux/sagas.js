import { takeLatest, call, put, all, select } from "redux-saga/effects";
import api from "./../../../services/api";
import {
  getBoardFailure,
  getBoardRequest,
  getBoardSuccess,
  getStatusFailure,
  getStatusRequest,
  getStatusSuccess,
  escolheCorFailure,
  escolheCorSuccess,
} from "./actions";

export function* getStatus() {
  try {
    const response = yield call(api.get, `/sequencia_cores/status/all`);

    const { status, score } = response.data;
    yield put(getBoardRequest());
    yield put(getStatusSuccess(status, score));
  } catch (err) {
    yield put(getStatusFailure());
    console.log(err);
  }
}

export function* getBoard() {
  try {
    const response = yield call(api.get, `/sequencia_cores/board`);

    yield put(getBoardSuccess(response.data));
  } catch (err) {
    yield put(getBoardFailure());
    console.log(err);
  }
}

export function* escolheCor({ payload }) {
  const { cor } = payload;
  try {
    const response = yield call(api.get, `/sequencia_cores/escolheCor/${cor}`);

    yield put(getStatusRequest());
    yield put(getBoardRequest());

    yield put(escolheCorSuccess());
  } catch (err) {
    yield put(escolheCorFailure());
  }
}

export default all([
  takeLatest("@seq/GET_STATUS_REQUEST", getStatus),
  takeLatest("@seq/GET_BOARD_REQUEST", getBoard),
  takeLatest("@seq/ESCOLHECOR_REQUEST", escolheCor),
]);
