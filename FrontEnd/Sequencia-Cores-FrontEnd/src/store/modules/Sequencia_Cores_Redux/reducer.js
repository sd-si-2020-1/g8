import produce from "immer";

const INITIAL_STATE = {
    board: [null, null, null, null,
            null, null, null, null,
            null, null, null, null],

    status: "aberto",
    score: 0,
    loading: false,
    escolheCorLoading: false,
};

export default function seq(state = INITIAL_STATE, action) {
    return produce(state, (draft) => {
        switch (action.type) {
            case "@seq/GET_STATUS_REQUEST": {
                draft.loading = true;
                break;
            }
        
            case "@seq/GET_STATUS_SUCCESS": {
                draft.status = action.payload.status;
                draft.score = action.payload.score;                
                draft.loading = false;
                break;
            }
        
            case "@seq/GET_STATUS_FAILURE": {
                draft.loading = false;
                break;
            }
        
            case "@seq/GET_BOARD_REQUEST": {
                draft.loading = true;
                break;
            }
        
            case "@seq/GET_BOARD_SUCCESS": {                
                draft.board = action.payload.board.toString();                
                draft.board = draft.board.split(",");
                
                for(var i = 0; i < 12; i++) {
                    switch(draft.board[i]) {
                        case null:
                            draft.board[i] = "#C4C4C4";
                            break;
                        case "amarelo":
                            draft.board[i] = "#FFFF00"; 
                            break;
                        case "laranja":
                            draft.board[i] = "#FFA500";
                            break;
                        case "verde":
                            draft.board[i] = "#0FF326";
                            break;
                        case "roxo":
                            draft.board[i] = "#EC1BE4";
                            break;
                        case "rosa":
                            draft.board[i] = "#FFC0CB";
                            break;
                        case "vermelho":
                            draft.board[i] = "#FF0000";
                            break;
                    }
                }
                draft.loading = false;
                break;
            }
        
            case "@seq/GET_BOARD_FAILURE": {
                draft.loading = false;
                break;
            }
        
            case "@seq/ESCOLHECOR_REQUEST": {
                draft.escolheCorLoading = true;
                break;
            }
        
            case "@seq/ESCOLHECOR_SUCCESS": {
                draft.escolheCorLoading = false;
                break;
            }
        
            case "@seq/ESCOLHECOR_FAILURE": {
                draft.escolheCorLoading = false;
                break;
            }    

        }
    });
}