import { all } from "redux-saga/effects";

import seq from "./Sequencia_Cores_Redux/sagas";

export default function* rootReducer() {
  return yield all([seq]);
}
