import React from "react";
import { StatusBar } from 'expo-status-bar';
import { Container } from './styles';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Titulo from "../../components/Titulo";

export default function App({navigation}) {
    return (
      <Container>
        <Titulo title="Sequência" top="-60px"/>
        <Titulo title="de" top="-50px"/>
        <Titulo title="Cores" top="-40px"/>
        <Titulo title="Cadastre-se" top="-10px"/>
        <Input title="Usuário" />
        <Input title="Senha" />
        <Button title="Cadastrar" 
            onPress={() => navigation.navigate("Screen1")}
        />
        <StatusBar style="auto" />
      </Container>
    );
  }