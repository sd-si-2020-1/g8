import React from "react";
import { StatusBar } from 'expo-status-bar';
import { Container } from './styles';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Titulo from "../../components/Titulo";

export default function App({navigation}) {
    return (
      <Container>
        <Titulo title="Sequência" top="-60px"/>
        <Titulo title="de" top="-50px"/>
        <Titulo title="Cores" top="-40px"/>
        <Titulo title="Conecte-se" top="-10px"/>
        <Input title="Usuário" />
        <Input title="Senha" />
        <Button title="Entrar" 
            onPress={() => navigation.navigate("Screen3")}
        />
        <Titulo title="Não tem conta?"></Titulo>
        <Button title="Cadastre-se" width="125px" height="30px" fontSize="16px"
            onPress={() => navigation.navigate("Screen2")}
          />                  
        <StatusBar style="auto" />
      </Container>
    );
  }