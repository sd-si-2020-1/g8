import React, { useEffect } from "react";
import { StatusBar } from 'expo-status-bar';
import { Container } from './styles';
import Square from '../../components/Square';
import ButtonSquare from '../../components/ButtonSquare'
import Titulo from "../../components/Titulo";
import { useSelector, useDispatch } from 'react-redux';
import { 
  escolheCorRequest,
  getBoardRequest,
  getStatusRequest } from "../../store/modules/Sequencia_Cores_Redux/actions";

export default function App() {
  const dispatch = useDispatch();

  const board = useSelector((state) => state.seq.board);
  const status = useSelector((state) => state.seq.status);
  const score = useSelector((state) => state.seq.score);  

  const LeftValue = (index) => {
    if (index === 0 || index === 4 || index === 8) return "10px";

    if (index === 1 || index === 5 || index === 9) return "100px";

    if (index === 2 || index === 6 || index === 10) return "190px";

    if (index === 3 || index === 7 || index === 11) return "280px";

  };

  const TopValue = (index) => {
    if (index < 4 ) return "69px";

    if (index < 8 ) return "157px";

    if (index < 12 ) return "245px";

  };

  useEffect(() => {
    dispatch(getStatusRequest());
    dispatch(getBoardRequest());    
  }, []);

    return (
      <Container>
        <Titulo title={"Pontuação: " +score} left="0px" top="0px" />
        <Titulo title={"Jogo: " +status} left="0px" top="1px" />

        {board.map((value, index) => {        
          return (
            <Square
              key={index}
              left={LeftValue(index)}
              top={TopValue(index)}
              color = {value}
            />
          );
        })}        
        

        <Titulo title="Cores disponíveis:" left="0px" top="275px" />

        <ButtonSquare color="#FFFF00" left="15px" top="398px"
          onPress={() => dispatch(escolheCorRequest("amarelo"))}
        />
        <ButtonSquare color="#FFA500" left="145px" top="398px"
          onPress={() => dispatch(escolheCorRequest("laranja"))}
        />
        <ButtonSquare color="#0FF326" left="275px" top="398px"
          onPress={() => dispatch(escolheCorRequest("verde"))}
        />

        <ButtonSquare color="#EC1BE4" left="15px" top="486px"
          onPress={() => dispatch(escolheCorRequest("roxo"))}
        />
        <ButtonSquare color="#FFC0CB" left="145px" top="486px"
          onPress={() => dispatch(escolheCorRequest("rosa"))}
        />
        <ButtonSquare color="#FF0000" left="275px" top="486px"
          onPress={() => dispatch(escolheCorRequest("vermelho"))}
        />

        <StatusBar style="auto" />
      </Container>
    );
  }