import axios from "axios";

export const { CancelToken, isCancel } = axios;

const api = axios.create({
  baseURL: "http://192.168.42.141:8080",
});

export default api;
